#!/usr/bin/env python3
from multiprocessing import Lock, Process, Event, Pipe
import time
from xml.etree.ElementTree import PI

import numpy as np


class TestProcessObject:

    def __init__(self):

        self.data_locker = Lock()
        self.id = 1
        self.data = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])
        self.read_pipe, self.write_pipe = Pipe(duplex=False)

    def read(self, data):

        while True:
            time.sleep(1)
            self.data = self.read_pipe.recv()
            print(data.id, "Read data", self.data)

    def write(self, data):

        while True:
            time.sleep(0.5)
            self.data += 1
            self.data %= 100
            if self.read_pipe.poll(0.1):
                print(data.id, "Have data don't read delete!",
                      self.read_pipe.recv())
            self.write_pipe.send(self.data)
            print(data.id, "Write data", self.data)


test_object = TestProcessObject()

read_process = Process(target=test_object.read, args=(test_object, ))
write_process = Process(target=test_object.write, args=(test_object, ))

read_process.start()
write_process.start()

while True:
    time.sleep(2)
    test_object.id += 1
    test_object.id %= 10

read_process.join()
write_process.join()