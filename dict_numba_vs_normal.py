#!/usr/bin/env python3

import numpy as np

import time

from numba import jit
import numba as nb
from numba.typed import Dict


@jit(nopython=True, fastmath=True)
def loop_numba_dict(voxel, keys):
    for idx_key in range(len(keys)):
        result = 0
        for idx in range(len(keys[idx_key])):
            result += keys[idx_key][idx]
        voxel[keys[idx_key]] = result


def loop_dict(voxel, keys):
    for idx_key in range(len(keys)):
        result = 0
        for idx in range(len(keys[idx_key])):
            result += keys[idx_key][idx]
        voxel[keys[idx_key]] = result


keys = (
    (1, 2, 3),
    (1, 2, 4),
    (5, 6, 7),
    (8, 9, 10),
    (11, 4, 3),
    (10, 2, 4),
    (2, 5, 7),
    (1, 28, 2),
    (8, 21, 1),
    (9, 100, 5),
    (10, 2, 50),
)

value = (
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
)

numba_dict = nb.typed.Dict.empty(
    key_type=nb.types.UniTuple(
        nb.types.float64,
        3,
    ),
    value_type=nb.types.float64,
)

normal_dict = dict()

for key, val in zip(keys, value):
    normal_dict[key] = val
    numba_dict[key] = val

print("Normal dict", normal_dict)
print("Numba dict", numba_dict)

iteration = 100000
list_time = []

for _ in range(iteration):
    t1 = time.time_ns()
    loop_numba_dict(numba_dict, keys)
    t2 = time.time_ns()
    list_time.append(t2 - t1)

data = np.array(list_time) / 1000
print(
    "Numba First round :\n\tmean %.3f micro-seconds\n\tmax %.3f\n\tmin %.3f\n\tstd %.3f\n\t%d iterations"
    % (
        np.mean(data),
        np.max(data),
        np.min(data),
        np.std(data),
        iteration,
    ))

list_time = []

for _ in range(iteration):
    t1 = time.time_ns()
    loop_numba_dict(numba_dict, keys)
    t2 = time.time_ns()
    list_time.append(t2 - t1)

data = np.array(list_time) / 1000
print(
    "Numba Second round :\n\tmean %.3f micro-seconds\n\tmax %.3f\n\tmin %.3f\n\tstd %.3f\n\t%d iterations"
    % (
        np.mean(data),
        np.max(data),
        np.min(data),
        np.std(data),
        iteration,
    ))

list_time = []

for _ in range(iteration):
    t1 = time.time_ns()
    loop_dict(normal_dict, keys)
    t2 = time.time_ns()
    list_time.append(t2 - t1)

data = np.array(list_time) / 1000
print(
    "Normal First round :\n\tmean %.3f micro-seconds\n\tmax %.3f\n\tmin %.3f\n\tstd %.3f\n\t%d iterations"
    % (
        np.mean(data),
        np.max(data),
        np.min(data),
        np.std(data),
        iteration,
    ))