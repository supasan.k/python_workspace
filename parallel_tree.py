import py_trees
import time

import multiprocessing


class MultiCoreParallel(py_trees.composites.Parallel):

    def tick(self):
        self.logger.debug("%s.tick()" % self.__class__.__name__)
        self.validate_policy_configuration()

        # reset
        if self.status != py_trees.common.Status.RUNNING:
            self.logger.debug("%s.tick(): re-initialising" %
                              self.__class__.__name__)
            for child in self.children:
                # reset the children, this ensures old SUCCESS/FAILURE status flags
                # don't break the synchronisation logic below
                if child.status != py_trees.common.Status.INVALID:
                    child.stop(py_trees.common.Status.INVALID)
            self.current_child = None
            # subclass (user) handling
            self.initialise()

        # nothing to do
        if not self.children:
            self.current_child = None
            self.stop(py_trees.common.Status.SUCCESS)
            yield self
            return

        # process them all first
        # for child in self.children:
        #     if self.policy.synchronise and child.status == py_trees.common.Status.SUCCESS:
        #         continue
        #     for node in child.tick():
        #         print(node)
        #         yield node
        #NOTE Edit for make multi processing
        # _list = []
        # for child in self.children:
        #     if child.status != py_trees.common.Status.RUNNING:
        #         child.initialise()
        #         # don't set child.status yet, terminate() may need to check what the current state is first
        #         process = multiprocessing.Process(target=child.update)
        #         process.start()
        #         _list.append([process, child])

        # for process, child in _list:
        #     process.join()
        #     new_status = child.result_status
        #     if new_status not in list(py_trees.common.Status):
        #         child.logger.error(
        #             "A behaviour returned an invalid status, setting to INVALID [%s][%s]"
        #             % (new_status, child.name))
        #         new_status = py_trees.common.Status.INVALID
        #     if new_status != py_trees.common.Status.RUNNING:
        #         child.stop(new_status)
        #     child.status = new_status
        #     yield child
        _list = []
        for child in self.children:
            process = multiprocessing.Process(target=child.tick_once)
            process.start()
            _list.append([process, child])

        for process, child in _list:
            process.join()

        # determine new status
        new_status = py_trees.common.Status.RUNNING
        self.current_child = self.children[-1]
        try:
            failed_child = next(
                child for child in self.children
                if child.status == py_trees.common.Status.FAILURE)
            self.current_child = failed_child
            new_status = py_trees.common.Status.FAILURE
        except StopIteration:
            if type(self.policy
                    ) is py_trees.common.ParallelPolicy.SuccessOnAll:
                if all([
                        c.status == py_trees.common.Status.SUCCESS
                        for c in self.children
                ]):
                    new_status = py_trees.common.Status.SUCCESS
                    self.current_child = self.children[-1]
            elif type(self.policy
                      ) is py_trees.common.ParallelPolicy.SuccessOnOne:
                successful = [
                    child for child in self.children
                    if child.status == py_trees.common.Status.SUCCESS
                ]
                if successful:
                    new_status = py_trees.common.Status.SUCCESS
                    self.current_child = successful[-1]
            elif type(self.policy
                      ) is py_trees.common.ParallelPolicy.SuccessOnSelected:
                if all([
                        c.status == py_trees.common.Status.SUCCESS
                        for c in self.policy.children
                ]):
                    new_status = py_trees.common.Status.SUCCESS
                    self.current_child = self.policy.children[-1]
            else:
                raise RuntimeError(
                    "this parallel has been configured with an unrecognised policy [{}]"
                    .format(type(self.policy)))
        # this parallel may have children that are still running
        # so if the parallel itself has reached a final status, then
        # these running children need to be terminated so they don't dangle
        if new_status != py_trees.common.Status.RUNNING:
            self.stop(new_status)
        self.status = new_status
        yield self


class SimpleBehavior(py_trees.behaviour.Behaviour):

    def __init__(self, name):
        super(SimpleBehavior, self).__init__(name)
        self.count = 1000000000
        self.result_status = py_trees.common.Status.INVALID

    def update(self):
        print("%s loop iter pass count %d" % (self.name, self.count))
        for i in range(self.count):
            pass
        print("%s Done count" % self.name)

        self.result_status = py_trees.common.Status.SUCCESS
        return py_trees.common.Status.SUCCESS

    def stop(self, new_status=py_trees.common.Status.INVALID):
        super(SimpleBehavior, self).stop(new_status)


root = MultiCoreParallel(
    "UseLessTree",
    py_trees.common.ParallelPolicy.SuccessOnAll(),
)

root.add_child(SimpleBehavior("first"))
root.add_child(SimpleBehavior("second"))
root.add_child(SimpleBehavior("third"))
root.add_child(SimpleBehavior("forth"))

tree = py_trees.trees.BehaviourTree(root)

print("Start Tick")
start = time.perf_counter()
tree.tick()
end = time.perf_counter()
print("Multi Core Parallel End Tick time %f" % (end - start))

root = py_trees.composites.Parallel(
    "UseLessTree",
    py_trees.common.ParallelPolicy.SuccessOnAll(),
)

root.add_child(SimpleBehavior("first"))
root.add_child(SimpleBehavior("second"))
root.add_child(SimpleBehavior("third"))
root.add_child(SimpleBehavior("forth"))

tree = py_trees.trees.BehaviourTree(root)

print("Start Tick")
start = time.perf_counter()
tree.tick()
end = time.perf_counter()
print("Single Core Parallel End Tick time %f" % (end - start))

root = py_trees.composites.Sequence("UseLessTree", )

root.add_child(SimpleBehavior("first"))
root.add_child(SimpleBehavior("second"))
root.add_child(SimpleBehavior("third"))
root.add_child(SimpleBehavior("forth"))

tree = py_trees.trees.BehaviourTree(root)

print("Start Tick")
start = time.perf_counter()
tree.tick()
end = time.perf_counter()
print("Single Core Sequence End Tick time %f" % (end - start))