import numpy as np

# X Y Value

data = np.array([[1, 2, 2], [1, 3, 3], [1, 4, 3], [1, 5, 3]])

new_data = np.array([[1, 2, 3], [4, 4, 3], [1, 5, 3]])

after_merge = np.array([[1, 2, 3], [1, 3, 3], [1, 4, 3], [1, 5, 3], [4, 4, 3]])
