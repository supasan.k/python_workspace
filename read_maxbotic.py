import serial

serialPort = serial.Serial(
    port="/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A56VT2OP-if00-port0",
    baudrate=9600,
    bytesize=serial.EIGHTBITS,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
)

serialString = ""


def Print_Binary_Values(num):
    # base condition
    if (num > 1):
        Print_Binary_Values(num // 2)
    print(num % 2, end="")

def read_ascii_data():
    data = serialPort.read(1)

    return data.decode("Ascii")

# MODE = ""
MODE = "active"

while (1):

    if MODE == "active":
        data = read_ascii_data()
        if data == "R":
            result = 0
            while (1):
                data = read_ascii_data()
                if data == "\r":
                    break
                result = ( result * 10 ) + int(data)
            print(result)
    else:
        data = serialPort.read(1)
        int_data = int.from_bytes(data, "big")
        print("Origin", data, "\tBinary is ", end="")
        Print_Binary_Values(int_data)
        try:
            ascii_data = data.decode("Ascii", )
        except:
            ascii_data = ""
        print("\t\tAscii is", ascii_data, end="")
        print()
