#!/usr/bin/env python3
import multiprocessing as mp
from threading import Thread
import time

import numpy as np

import os


class TestProcessObject:

    def __init__(self):

        self.data_locker = mp.Lock()
        self.data = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])

    def read(self):

        while True:
            time.sleep(1)
            with self.data_locker:
                print(os.getpid(), self.data, self)

    def write(self):

        while True:
            time.sleep(0.1)
            with self.data_locker:
                self.data += 1
                self.data %= 100
                print(os.getpid(), "Add data", self.data, self)


if __name__ == "__main__":
    mp.set_start_method('fork')

    test_object = TestProcessObject()
    read_process = mp.Process(target=test_object.read)
    write_process = mp.Process(target=test_object.write)

    # read_process = Thread(target=test_object.read)
    # write_process = Thread(target=test_object.write)

    read_process.start()
    write_process.start()

    read_process.join()
    write_process.join()