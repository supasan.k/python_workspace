# 0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181
def gen_fibo(n_times):
    num1 = 0
    num2 = 1
    yield num1
    yield num2
    for _ in range(2, n_times):
        result = num1 + num2
        num1 = num2
        num2 = result
        yield result


for i in gen_fibo(20):
    print(i, end=" ")
