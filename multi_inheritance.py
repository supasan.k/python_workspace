#!/usr/bin/env python3


class A:

    def __init__(self):
        pass

    def print(self):
        print("Print from A")

    def special_a(self):
        print("hi a")


class B:

    def __init__(self):
        pass

    def print(self):
        print("Print from B")

    def special_b(self):
        print("hi b")


class C(B, A):

    def __init__(self):
        pass


if __name__ == "__main__":

    c = C()
    c.print()
    c.special_a()
    c.special_b()