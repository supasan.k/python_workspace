#!/usr/bin/env python3
import multiprocessing as mp
from multiprocessing import managers
from multiprocessing.managers import BaseManager, SharedMemoryManager
from threading import Thread
import time

import numpy as np

import os


class TestProcessObject:

    def __init__(self):

        self.data_locker = mp.Lock()
        self.data = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])

    def read(self):

        print("Read Acquire Lock at", time.time())
        with self.data_locker:
            print("Read in Lock i sleep 5 second", time.time())
            time.sleep(5)
        print("Read release", time.time())

    def write(self):
        print("Write Acquire Lock at", time.time())
        with self.data_locker:
            print("Write in Lock i sleep 5 second", time.time())
            time.sleep(5)
        print("Write release", time.time())


if __name__ == "__main__":
    BaseManager.register("TestProcessObject", TestProcessObject)

    manager = BaseManager()
    manager.start()

    test_object = manager.TestProcessObject()

    read_process = mp.Process(target=test_object.read)
    write_process = mp.Process(target=test_object.write)

    # read_process = Thread(target=test_object.read)
    # write_process = Thread(target=test_object.write)

    read_process.start()
    write_process.start()

    read_process.join()
    write_process.join()