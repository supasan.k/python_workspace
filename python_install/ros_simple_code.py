#!/usr/bin/env python3

from std_msgs.msg import Header
from hg_std_msgs.msg import StringStamped
from hg_std_msgs.srv import StringService

from hg_pyutil.node_handle import NodeHandle

if __name__=="__main__":
    nh = NodeHandle( "simple_publish" )
    rate = nh.create_rate(10)
    run = 0 
    publisher = nh.create_publisher( "/string_topic", StringStamped, queue_size = 1)
    header = Header()
    header.frame_id = 'tester'
    while nh.ok():
        rate.sleep()
        run = ( run + 1 ) % 10
        header.stamp = nh.now( msg = True )
        publisher.publish( StringStamped( header, f'Run number {run}'))
    

