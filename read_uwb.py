import serial
import time

serialPort = serial.Serial(
    port="/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_AB0NRIQT-if00-port0",
    baudrate=115200,
    bytesize=serial.EIGHTBITS,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    timeout = 1.0
)

def get_response( loop_count ):
    _result = True
    response = []
    for idx in range( loop_count ):
        result = list( serialPort.read(2) )
        if PRINT_CONNECTION : print( "TL :", result )
        if len( result ) != 2 : _result = False
        else :
            response += result
            length = result[1]
            result = list( serialPort.read( length ) )
            if PRINT_CONNECTION : print( "Value :", len(result), ":" , result )
            if len( result ) != length : _result = False
            else : response += result

        if _result == False:
            break

    return _result, response

first_time = True
PRINT_CONNECTION = False
success = False
last_time = time.time()

restart_time = time.time()
restart_period = 3

period = 50
last_time_okay = time.time() - period
start_time = time.time()
while( 1 ):
    time.sleep(0.1)
    result = serialPort.write( bytearray([0x32,0x00]))
    if PRINT_CONNECTION : print( "Write x32 x00 result : ", result )
    result = serialPort.read(7)
    if PRINT_CONNECTION : print( "Result length", len(result), list( result ) )
    if len( result ) != 7 :
        if success == True:
            print( f'Start disapper {time.time() - start_time:.4f} seconds' )
            last_time = time.time()
            success = False
        diff_time = time.time()  - last_time
        if diff_time > 1.0 and first_time == False:
            if time.time() - restart_time < restart_period :
                continue 
            print( 'Disapper over 1 second set DTR is False', time.time() - start_time )
            serialPort.dtr = False
            time.sleep(0.01)
            print( "Wake up and set DTR is True", time.time() - start_time )
            serialPort.dtr = True
            time.sleep(2.0)
            print( "Port is open?", serialPort.is_open, time.time() - start_time )
            restart_time = time.time()
            continue
        first_time = False
    else:
        if success == False:
            print( f'After disapper : {time.time() - last_time:.4f} seconds comback on { time.time() - start_time:.4f} seconds')
        success = True
    first_time = False

    result = serialPort.write( bytearray([0x0c,0x00]))
    if PRINT_CONNECTION : print( "Write x0c x00 result : ", result )
    _success, response = get_response( 3 )
    if _success == True:
        if time.time() - last_time_okay > period:
            print( f'Still running {time.time() - start_time:.4f} seconds' )
            last_time_okay = time.time()
