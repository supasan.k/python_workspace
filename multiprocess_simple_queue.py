#!/usr/bin/env python3
from multiprocessing import Lock, Process, Event, SimpleQueue
import time
from xml.etree.ElementTree import PI

import numpy as np


class TestProcessObject:

    def __init__(self):

        self.data_locker = Lock()
        self.data = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])
        self.data_queue = SimpleQueue()

    def read(self):

        while True:
            time.sleep(1)
            self.data = self.data_queue.get()
            print(self.data)

    def write(self):

        while True:
            time.sleep(0.1)
            self.data += 1
            self.data %= 100
            self.data_queue.put(self.data)
            print("Write data", self.data)


test_object = TestProcessObject()

read_process = Process(target=test_object.read)
write_process = Process(target=test_object.write)

read_process.start()
write_process.start()

read_process.join()
write_process.join()