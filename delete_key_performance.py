#REFRENCE : https://stackoverflow.com/questions/22668574/python-fastest-strategy-for-remove-a-lot-of-keys-from-dict

import cProfile, pstats

from io import StringIO
import numpy as np
import sys

import atexit
import line_profiler
from functools import partial

profile = line_profiler.LineProfiler()
atexit.register( partial(profile.print_stats,output_unit=1e-03) )

pr = cProfile.Profile()

# size_dict = 10000000
size_dict = int(input("Size of dict: " ))

print( "Creating dict size :", size_dict )
original_dict = {item: np.ones(7) for item in range(size_dict)}
print( "Dict size : %d get size %.4f GB"%(
    size_dict, 
    original_dict.__sizeof__() / 1024 / 1024 / 1024, 
    ))


delete_size = int(input("Key size to delete: "))
delete_key = np.random.choice( size_dict, delete_size, replace = False ).tolist()


pr.enable()
# guard_condition = int(raw_input("Enter guard_condition: "))
# new_d = {key: value for key, value in d.iteritems() if key >= guard_condition }
# def del_iter(d, guard_condition):
#     for key in d.keys():
#         if key < guard_condition:
#             del d[key]
# del_iter(original_dict, guard_condition)
@profile
def del_compression(original_dict):
    compression_dict = {key : value for key, value in original_dict.items() if key not in delete_key }
    return compression_dict
print( "Compression process")
compression_dict = del_compression(original_dict)

@profile
def del_iter(d, delete_key ):
    for key in delete_key:
        del d[key]
print( "Iteration process")
del_iter(original_dict, delete_key)




pr.disable()
print("Length dict compression %d iter %d"%( len(compression_dict), len(original_dict)) )
s = StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print( s.getvalue() )
