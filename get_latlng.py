import pyexiv2
from pyexiv2.utils import Fraction

def dcm2decimal( dd : Fraction , mm : Fraction , ss : Fraction ):
     return float(dd) + float(mm) / 60 + float(ss) / 3600

def to_latlng( lat_deg, lng_deg ):
    lat = dcm2decimal(lat_deg[0][0], lat_deg[0][1], lat_deg[0][2])
    lng = dcm2decimal(lng_deg[0][0], lng_deg[0][1], lng_deg[0][2])
    if lat_deg[1] == "S":
        lat *= -1.0
    if lng_deg[1] == "W":
        lat *= -1.0
    return lat, lng

def get_latlng( img_path ):
    metadata = pyexiv2.ImageMetadata( img_path )
    metadata.read()
    lat_deg = (
            metadata["Exif.GPSInfo.GPSLatitude"].value,
            metadata["Exif.GPSInfo.GPSLatitudeRef"].value
    )
    lng_deg = (
            metadata["Exif.GPSInfo.GPSLongitude"].value,
            metadata["Exif.GPSInfo.GPSLongitudeRef"].value,
    )
    return to_latlng( lat_deg, lng_deg )

DECIMAL_POINT = 8
def to_deg(value, loc):
    if value < 0:
        loc_value = loc[0]
    elif value > 0:
        loc_value = loc[1]
    else:
        loc_value = ""
    abs_value = abs(value)
    deg = int(abs_value)
    t1 = (abs_value - deg) * 60
    min = int(t1)
    sec = round((t1 - min) * 60, DECIMAL_POINT)
    return (deg, min, sec, loc_value)

