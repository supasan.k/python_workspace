import serial
import numpy as np
import time

if True:
    serialPort = serial.Serial(
        port="/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A56VT2OP-if00-port0",
        baudrate=9600,
        bytesize=serial.EIGHTBITS,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
    )

def Print_Binary_Values(num):
    # base condition
    if (num > 1):
        Print_Binary_Values(num // 2)
    print(num % 2, end="")

def check_sum( package ):
    
    check_value = ( package[0] + package[1] + package[2] ) & 0xFF

    print( "Sum ", hex(check_value), end = " ")
    print( "check", hex(package[3] & 0xFF),end = " ")
    print()
    return check_value == package[3] & 0xFF

def get_value( package ):
    distance = ( int( package[1] ) * 256 ) + int( package[2] )
    return distance
    # print( package[1], package[2])
    # high_byte = int.to_bytes( int( package[1] ) & 0xFF,1, "big" )
    # low_byte = int.to_bytes( int( package[2] ) & 0xFF, 1, "big")
    # print( high_byte, low_byte, high_byte + low_byte)
    # return int.from_bytes( high_byte + low_byte, "big" )

def capture_package():
    package = np.zeros(4, dtype = np.ubyte ) 

    idx = 0
    while idx < 4: 
        # print( "Try read")
        data = serialPort.read(1)
        # print( "result ", data, int.from_bytes( data, "big" ))
        data_correct = False
        data = int.from_bytes( data, "big" )
        if idx == 0:
            if data == 0xFF:
                data_correct = True
        else:
            data_correct = True

        if data_correct == True:
            # print( "idx", idx, "add data", hex(data))
            package[idx] = data
            idx += 1
        else:
            pass

    return package

if __name__=="__main__":
    while (1):
        time.sleep( 0.1 ) 
        package = capture_package()
        package_correct = check_sum( package )
        print( "Input : ", end="")
        for idx in range( len(package) ):
            print( '',  hex( package[idx]), end="" )
        if package_correct == True :
            print( "Distance", get_value(package), "mm")
        else:
            print( "Failure check sum")
