#!/usr/bin/env python3

import enum


class A(enum.Enum):
    """
        Left Hand Side is name
        Right Hand Side is value
    """

    CROSS = 1
    cross = 1


print("A[\'cross\']", A['cross'])
print("A[\'CROSS\']", A["CROSS"])
print("A(1)", A(1))
print(f'name {A["cross"].name} value {A["cross"].value}')
print(f'name {A["CROSS"].name} value {A["CROSS"].value}')
