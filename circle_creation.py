#!/usr/bin/env python3

import typing

import numpy as np


def circle_check_points(points: typing.Tuple[typing.Tuple[typing.Union[float,
                                                                       int]],
                                             complex]):
    same = False
    for idx1 in range(len(points)):
        for idx2 in range(idx1 + 1, len(points)):
            same = points[idx1] == points[idx2]
            if same == True:
                break
        if same == True:
            break

    return same


def circle_solution_complex(
    p1: typing.Tuple[typing.Union[float, int]],
    p2: typing.Tuple[typing.Union[float, int]],
    p3: typing.Tuple[typing.Union[float, int]],
) -> typing.Tuple[typing.Tuple[float], float]:
    z1 = complex(p1[0], p1[1])
    z2 = complex(p2[0], p2[1])
    z3 = complex(p3[0], p3[1])

    if circle_check_points((z1, z2, z3)):
        raise ValueError(f'Duplicate points: {z1}, {z2}, {z3}')

    w = (z3 - z1) / (z2 - z1)

    if w.imag == 0:
        raise ValueError(f'Points are collinear : {z1}, {z2}, {z3}')

    c = (z2 - z1) * (w - abs(w)**2) / (2j * w.imag) + z1
    r = abs(z1 - c)

    return (c.real, c.imag), r


def circle_solution_matrix(
    p1: typing.Tuple[typing.Union[float, int]],
    p2: typing.Tuple[typing.Union[float, int]],
    p3: typing.Tuple[typing.Union[float, int]],
) -> typing.Tuple[typing.Tuple[float], float]:
    mat_a = np.array([
        [p1[0] * p1[0] + p1[1] * p1[1], p1[0], p1[1], 1],
        [p2[0] * p2[0] + p2[1] * p2[1], p2[0], p2[1], 1],
        [p3[0] * p3[0] + p3[1] * p3[1], p3[0], p3[1], 1],
    ])

    x = 0.5 * np.linalg.det(np.delete(mat_a, 1, 1)) / np.linalg.det(
        np.delete(mat_a, 0, 1))
    y = 0.5 * np.linalg.det(np.delete(mat_a, 2, 1)) / np.linalg.det(
        np.delete(mat_a, 0, 1))
    r = np.sqrt(x * x + y * y + np.linalg.det(np.delete(mat_a, 3, 1)) /
                np.linalg.det(np.delete(mat_a, 0, 1)))

    return ((x, y), r)


if __name__ == '__main__':
    points = [[1.0, 0.0], [0.0, -1.0], [-1.0, 0.0]]

    result_complex = circle_solution_complex(points[0], points[1], points[2])
    print("complex", result_complex)
    result_matrix = circle_solution_matrix(points[0], points[1], points[2])
    print("matrix", result_matrix)

    points = [[3.0, 4.0], [3.0, 3.0], [2.0, 4.0]]

    result_complex = circle_solution_complex(points[0], points[1], points[2])
    print("complex", result_complex)
    result_matrix = circle_solution_matrix(points[0], points[1], points[2])
    print("matrix", result_matrix)
