import time

import multiprocessing
import threading


def useless_function(num, count=100000000):
    print("%d loop iter pass count %d" % (num, count))
    for i in range(count):
        pass
    print("%d Done count" % num)


start = time.perf_counter()
useless_function(1)
useless_function(2)
useless_function(3)
# useless_function(4)
end = time.perf_counter()
print("Original solution %f" % (end - start))

start = time.perf_counter()
process1 = multiprocessing.Process(target=useless_function, args=(1, ))
process2 = multiprocessing.Process(target=useless_function, args=(2, ))
process3 = multiprocessing.Process(target=useless_function, args=(3, ))
# process4 = multiprocessing.Process(target=useless_function, args=(4, ))
process1.start()
process2.start()
process3.start()
# process4.start()
process1.join()
process2.join()
process3.join()
# process4.join()
end = time.perf_counter()
print("Multiprocessing solution %f" % (end - start))

start = time.perf_counter()
process1 = threading.Thread(target=useless_function, args=(1, ))
process2 = threading.Thread(target=useless_function, args=(2, ))
process3 = threading.Thread(target=useless_function, args=(3, ))
# process4 = threading.Thread(target=useless_function, args=(4, ))
process1.start()
process2.start()
process3.start()
# process4.start()
process1.join()
process2.join()
process3.join()
# process4.join()
end = time.perf_counter()
print("Threading solution %f" % (end - start))
