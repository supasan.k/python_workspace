#!/usr/bin/env python3

import numpy as np

from tf import transformations

from geometry_msgs.msg import PoseStamped, Point, Quaternion
from ar_track_alvar_msgs.msg import AlvarMarker
from hg_navigation.ar_marker import MarkerBundle

bundle = MarkerBundle( 1, "odom" )
bundle.add_child(
    2,
    np.array([3.0, -0.5, 0.0]),
    transformations.quaternion_from_euler( np.pi / 2, 0.0, -np.pi / 2),
)
bundle.add_child(
    3,
    np.array([3.0, 4.0, 0.0]),
    transformations.quaternion_from_euler( np.pi, 0.0, np.pi / 2),
)

marker_2 = AlvarMarker()
marker_2.header.frame_id = "odom"
marker_2.id = 2
marker_2.pose.header.frame_id = "odom"
marker_2.pose.pose.position = Point( 3.0, 4.5, 0.0)
marker_2.pose.pose.orientation = Quaternion( *transformations.quaternion_from_euler(
    -np.pi /2,
    0.0,
    np.pi / 2 ))
bundle.update_marker( marker_2 )
# print( "Marker_2", marker_2 )

marker_3 = AlvarMarker()
marker_3.header.frame_id = "odom"
marker_3.id = 3
marker_3.pose.header.frame_id = "odom"
marker_3.pose.pose.position = Point( 3.0, 0.0, 0.0)
marker_3.pose.pose.orientation = Quaternion( *transformations.quaternion_from_euler(
    0.0,
    0.0,
    -np.pi / 2 ))
bundle.update_marker( marker_3 )
# print( "Marker_3", marker_3 )

bundle.info(True, True)
print( "Result", bundle.get_marker() )
result = bundle.get_average_candidate()
print( "avg position", result[0], "euler", transformations.euler_from_quaternion( result[1]))
