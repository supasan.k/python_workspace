#!/usr/bin/env python3
from cgi import test
import multiprocessing as mp
from multiprocessing import managers
from multiprocessing.managers import BaseManager, SharedMemoryManager
from threading import Thread
import time

import numpy as np

import os

event_read = mp.Event()
event_write = mp.Event()


class TestProcessObject:

    def __init__(self):

        self.data_locker = mp.Lock()
        self.data = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])

    def read(self):

        while event_read.is_set():
            time.sleep(1)
            with self.data_locker:
                print(os.getpid(), self.data, self)

    def write(self):

        while event_write.is_set():
            time.sleep(0.1)
            with self.data_locker:
                self.data += 1
                self.data %= 100
                print(os.getpid(), "Add data", self.data, self)


if __name__ == "__main__":
    BaseManager.register("TestProcessObject", TestProcessObject)

    manager = BaseManager()
    manager.start()

    test_object = manager.TestProcessObject()

    event_write.set()
    event_read.set()

    read_process = mp.Process(target=test_object.read)
    write_process = mp.Process(target=test_object.write)

    # read_process = Thread(target=test_object.read)
    # write_process = Thread(target=test_object.write)

    read_process.start()
    write_process.start()

    read_process.join()
    write_process.join()