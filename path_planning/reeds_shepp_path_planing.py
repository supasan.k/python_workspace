#!/usr/bin/env python3

from hg_navigation.path_planning.reeds_shepp_path import reeds_shepp_path_planning
from hg_navigation.path_tracking.stanley_controller import calc_path_offset

import numpy as np
np.set_printoptions(precision=2, suppress=True, linewidth=150)

start_pose = np.array([
    [ 0., 0., 0., 0., 0., 0.],
    [ 0., 0., 0., 0., 0., 0.],
    [ 0., 0., 0., 0., 0., 0.],
    [ 0., 0., 0., 0., 0., 0.],
] )

target_pose = np.array([
    [ -2., 0., 0., 0., 0., 0.],
    [ -2., 0., 0., 0., 0., 1.57],
    [ 0., 0., 0., 0., 0., 3.14],
    [ -2., -2., 0., 0., 0., 1.57],
] )

for idx in range( len( start_pose )):
    result = reeds_shepp_path_planning(
            start_pose[idx][0],
            start_pose[idx][1],
            start_pose[idx][5],
            target_pose[idx][0],
            target_pose[idx][1],
            target_pose[idx][5],
            0.5,
            0.1,
        )
    print( f'===============> Case id {idx}/{len(start_pose) - 1 }')
    print( "start pose", start_pose[idx] )
    print( "target pose", target_pose[idx] )
    if result[0] is not None:
        path = result[3]
        print( "Orignal Path")
        print( path )

#        path_offset = calc_path_offset( path, np.array([0.5, 0.], dtype = np.float64 ) )
#        print( "Offset Path")
#        print( path_offset)

        print("Mode ", result[0])
        print('path length', len( path), "trig on idx", result[2])
    else:
        print( "Can find path")

