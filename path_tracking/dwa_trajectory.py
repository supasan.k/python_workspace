#!/usr/bin/env python3

import numpy as np

np.set_printoptions(precision=2,
                    suppress=True,
                    linewidth=200,
                    threshold=100000)

from hg_navigation.path_planning.reeds_shepp_path import reeds_shepp_path_planning
from hg_navigation.path_tracking import dwa_trajectory_with_grid as Specific
from hg_navigation import path_tracking as Base

start_pose = np.array([
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 1., 0., 0., 0., 0.],
    [0., 5., 0., 0., 0., 0.],
])

target_pose = np.array([
    [50., 0., 0., 0., 0., 0.],
    [-2., 0., 0., 0., 0., 0.],
    [10., 10., 0., 0., 0., 1.57],
    [-2., -2., 0., 0., 0., 1.57],
    [-2., 0., 0., 0., 0., 1.57],
    [0., 0., 0., 0., 0., 3.14],
    [10., -5., 0., 0., 0., 1.57],
    [10., -5., 0., 0., 0., -1.57],
    [-6., -2., 0., 0., 0., -3.14],
    [5., 6., 0., 0., 0., 1.57],
    [30., 35., 0., 0., 0., 1.57],
])

obstacle_set = [
    np.array([[25., 0.]]),
    np.array([]),
    np.array([]),
    np.array([]),
    np.array([]),
    np.array([]),
    np.array([]),
    np.array([]),
    np.array([]),
    np.array([[3., 3.]]),
    np.array([[18., 20.]]),
]

#NOTE Start at 9
#NOTE Long at Start + 1
target_idx = 0

footprint_polygon = np.array([
    [-5.0, -3.0],
    [-5.0, 3.0],
    [5.0, 3.0],
    [5.0, -3.0],
    [-5.0, -3.0],
])

for idx in range(target_idx, target_idx + 1):
    # for idx in range( len(start_pose) ):

    vehicle: Specific.Vehicle = Specific.Vehicle()
    vehicle[Base.Key.vx_max] = 2.0
    vehicle[Specific.Key.limit_path_trajectory] = 40.0
    vehicle[Specific.Key.path_alignment_distance_ok] = 0.05
    vehicle[Specific.Key.path_alignment_distance_limit] = 50.0
    vehicle[Specific.Key.turn_radius_min] = 5.0

    vehicle[Specific.Key.cost_gain_obstacle] = -1.0
    vehicle[Specific.Key.cost_gain_path_access] = 10.0
    vehicle[Specific.Key.cost_gain_path_alignment] = 10.0
    vehicle[Specific.Key.cost_gain_path_alignment_max] = -10.0
    vehicle[Specific.Key.cost_gain_path_alignment_min] = 6.0

    cross_track_p = 3.0
    max_curvature = 1.0 / (vehicle[Specific.Key.turn_radius_min] + 1.0)
    wheel_base = 0.295

    print("Finding Path")
    result = reeds_shepp_path_planning(
        start_pose[idx][0],
        start_pose[idx][1],
        start_pose[idx][5],
        target_pose[idx][0],
        target_pose[idx][1],
        target_pose[idx][5],
        max_curvature,
        1.0,
    )
    if result[0] is not None:
        path = result[3]
        # print( result[0], result[1], result[2])
        # print( path )
    else:
        print("Can find path")

    gif_name = f'dwa_trajectory'
    gif_name += f'_{idx:02d}'
    gif_name += f'_turnradius{vehicle[Specific.Key.turn_radius_min]}'
    gif_name += f'_predict{vehicle[Specific.Key.limit_path_trajectory]}'
    gif_name += f'_ok{vehicle[Specific.Key.path_alignment_distance_ok]}'

    print("Simulationing")
    time_capture, sr = Specific.simulation(
        vehicle,
        path,
        0.1,
        60,
        realtime_review=True,
        ending_review=False,
        gif_name=gif_name,
        obstacle=obstacle_set[idx],
        footprint_polygon=footprint_polygon,
    )
    print(f'====== Result Test Case number {idx:03d} ======')
    print("Start Pose  :", start_pose[idx])
    print("Target Pose :", target_pose[idx])
    print(f'Result In time {time_capture:.3f} from {len(path)}')
