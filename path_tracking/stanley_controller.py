#!/usr/bin/env python3

import numpy as np

np.set_printoptions(precision=2,
                    suppress=True,
                    linewidth=200,
                    threshold=100000)

from hg_navigation.path_planning.reeds_shepp_path import reeds_shepp_path_planning
from hg_navigation.path_tracking.stanley_controller import simulation, Vehicle

start_pose = np.array([
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
    [0., 0., 0., 0., 0., 0.],
])

target_pose = np.array([
    [2., 0., 0., 0., 0., 0.],
    [-2., 0., 0., 0., 0., 0.],
    [2., 2., 0., 0., 0., 1.57],
    [-2., -2., 0., 0., 0., 1.57],
    [-2., 0., 0., 0., 0., 1.57],
    [0., 0., 0., 0., 0., 3.14],
    [10., -5., 0., 0., 0., 1.57],
    [10., -5., 0., 0., 0., -1.57],
    [-6., -2., 0., 0., 0., -3.14],
])

target_idx = 2
for idx in range(target_idx, target_idx + 1):
    # for idx in range( len(start_pose) ):
    cross_track_p = 3.0
    max_curvature = 1.5
    wheel_base = 0.295

    result = reeds_shepp_path_planning(
        start_pose[idx][0],
        start_pose[idx][1],
        start_pose[idx][5],
        target_pose[idx][0],
        target_pose[idx][1],
        target_pose[idx][5],
        max_curvature,
        0.1,
    )
    if result[0] is not None:
        path = result[3]
        # print( result[0], result[1], result[2])
        # print( path )
    else:
        print("Can find path")

    mode = "tracking_predict"
    # mode = "tracking_direction"
    # mode = "tracking"

    vehicle = Vehicle(
        wheel_base=wheel_base,
        max_curvature=max_curvature,
        cross_track_p=cross_track_p,
        vx_max=3.0,
    )

    time_capture, sr = simulation(
        vehicle,
        path,
        0.1,
        60,
        mode=mode,
        path_hint=result[2],
        realtime_review=True,
        ending_review=False,
        gif_name=
        f'{idx:02d}_{mode}_{cross_track_p:.3f}_{max_curvature:.2f}_{wheel_base:.2f}_offset',
    )
    print(f'====== Result Test Case number {idx:03d} ======')
    print("Start Pose  :", start_pose[idx])
    print("Target Pose :", target_pose[idx])
    print(f'Result In time {time_capture:.3f} from {len(path)}')
    print(
        f'Cross Path Peak  : {np.max(np.abs( sr.distance_capture ) ):7.3f} {np.min( np.abs( sr.distance_capture)):7.3f}'
    )
    print(
        f'Front Cross Peak : {np.max(np.abs( sr.distance_front_capture ) ):7.3f} {np.min( np.abs( sr.distance_front_capture)):7.3f}'
    )
    print(
        f'Steering Peak    : {np.max(np.abs( sr.theta_delta_capture ) ):7.3f} {np.min( np.abs( sr.theta_delta_capture)):7.3f}'
    )
    print(
        f'v_x Peak         : {np.max(np.abs( sr.twist_capture[:,0] ) ):7.3f} {np.min( np.abs( sr.twist_capture[:,0])):7.3f}'
    )
    print(
        f'v_yaw Peak       :{np.max(np.abs( sr.twist_capture[:,5] ) ):7.3f} {np.min( np.abs( sr.twist_capture[:,5])):7.3f}'
    )
    # print( np.c_[ state_capture[:,0], state_capture[:,1], state_capture[:,5],
    #     state_capture[:,6 + 0],state_capture[:, 6 + 5 ],
    #     state_capture[:,12 + 0]])
