#!/usr/bin/env python3
from cgi import test
import multiprocessing as mp
from multiprocessing import managers
from multiprocessing.managers import BaseManager, SharedMemoryManager
from threading import Thread
import time

import numpy as np

import os

from psutil import Process


class DataCenter:

    def __init__(self):

        self.data_locker = mp.Lock()
        self.data = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])


def read(data):

    while True:
        time.sleep(2)
        print("Reader", data.data)


def write(data):

    while True:
        time.sleep(1)
        data.data += 1
        data.data %= 100
        print("Write", data.data)


if __name__ == "__main__":

    data = DataCenter()

    reader = mp.Process(target=read, args=(data, ))
    writer = mp.Process(target=write, args=(data, ))

    reader.start()
    writer.start()

    writer.join()
    reader.join()
