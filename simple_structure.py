class Simple:

    def __len__(self):
        print( "Call len" )

    def __get__(self, instance ):
        print( "Call get parameter", instance)

    def __set__(self, instance, value ):
        print( "Call set parameter", instance, value )

    def __getitem__(self, key ):
        print( "Call getitem parameter", key )

    def __setitem__(self, key, value ):
        print( "Call setitem parameter", key, value )

    def __delitem__(self, key ):
        print( "Call delitem parameter", key )
