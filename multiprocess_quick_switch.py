#/usr/bin/env python3

from multiprocessing import shared_memory
import multiprocessing as mp
import time
import numpy as np

def process_read( shm : shared_memory.SharedMemory, event_in : mp.Event, event_out : mp.Event ):
    data = np.array([0], np.uint8)
    while data[0] < 20:

        time.sleep(0.0001)
        
        event_in.wait()
        event_in.clear()

        data[0] += 1

        buf = shm.buf
        buf[:1] = data.tobytes()

        event_out.set()

size_process = 2

shm1 = shared_memory.SharedMemory("/test1", create=True, size=1)
shm2 = shared_memory.SharedMemory("/test2", create=True, size=1)

event_in1 = mp.Event()
event_in2 = mp.Event()
event_out1 = mp.Event()
event_out2 = mp.Event()

process1 = mp.Process( target = process_read, args=(shm1,event_in1,event_out1))
process2 = mp.Process( target = process_read, args=(shm2,event_in2,event_out2))
process1.start()
process2.start()
    
result1 = 0
result2 = 0
while result1 < 20 or result2 < 20  :

    event_in1.set()
    event_in2.set()

    event_out1.wait()
    event_out1.clear()
    event_out2.wait()
    event_out2.clear()

    buf = shm1.buf
    data = np.frombuffer( buf[:1], np.uint8 )
    result1 = data[0]
    buf = shm2.buf
    data = np.frombuffer( buf[:1], np.uint8 )
    result2 = data[0]
    # print( f'result {result1:2d} {result2:2d}')

shm1.close()
shm2.close()
